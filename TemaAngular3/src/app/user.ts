export interface User {
  id: number;
  name: string;
  email: string;
  address: string;
  phone: number;
  website: string;
}

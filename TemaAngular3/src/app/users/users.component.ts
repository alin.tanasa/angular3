import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [{ id: 1, name: 'Alin', email: 'alin@e.com', phone: 0, address: 'lorem ipsum', website: 'e.com' },
  { id: 2, name: 'Jhon', email: 'jhon@e.com', phone: 0, address: 'lorem ipsum', website: 'home.com' },
  { id: 3, name: 'Andrew', email: 'andrew@e.com', phone: 0, address: 'lorem ipsum', website: 'ea.com' }];
  user: User = { id: 0, name: '', email: '', phone: 0, address: '', website: '' };

  constructor() { }

  ngOnInit(): void {
  }

  OnSearch(nume: string): void {
    for (let i = 0; i < this.users.length; i++) {
      if (nume == this.users[i].name) {
        this.user = this.users[i];
      }
    }
  }
}
